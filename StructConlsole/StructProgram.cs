﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace StructConlsole {

    internal class StructProgram {

        private const ConsoleColor defTextColor = ConsoleColor.White;
        private const ConsoleColor errorColor = ConsoleColor.Red;
        private const ConsoleColor titleColor = ConsoleColor.Green;
        private const ConsoleColor hintColor = ConsoleColor.Yellow;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            Console.Title = "Лабораторна робота No4. Завдання 1";
            Console.ForegroundColor = defTextColor;
            start(new Student[0]);
        }

        private static void start(Student[] items) {
            printMenuTitle("Меню");
            int select = selectMenuItem(new string[] {
                "Вихід",
                "створити студентів",
                "показати список студентів",
                "показати студента по номеру",
                "найвищий та найнижчий середній бал",
                "відсортувати студентів за середнім балом",
                "відсортувати студентів за ім'ям",
            });

            Console.Clear();
            switch (select) {
                case 0:
                    return;
                case 1:
                    printMenuTitle("створити студентів");
                    items = ReadStudentsArray();
                    Console.Clear();
                    break;
                case 2:
                    printMenuTitle("список студентів");
                    PrintStudents(items);
                    break;
                case 3:
                    printMenuTitle("інформація про студента");
                    if (items == null || items.Length < 1) writeLine("Список студентів порожній!!!", hintColor);
                    else PrintStudent(items, inputInt($"номер студента від 1 до {items.Length}", 1, items.Length));
                    break;
                case 4:
                    printMenuTitle("найвищий та найнижчий середній бал");
                    if (items == null || items.Length < 1) {
                        writeLine("Список студентів порожній!!!", hintColor);
                    } else {
                        double max;
                        double min;
                        GetStudentsInfo(items, out min, out max);
                        write("найвищий бал : ");
                        writeLine(max.ToString(), hintColor);
                        write("найнижчий бал : ");
                        writeLine(min.ToString(), hintColor);
                    }
                    break;
                case 5:
                    printMenuTitle("відсортувати студентів за середнім балом");
                    SortStudentsByPoints(items);
                    break;
                case 6:
                    printMenuTitle("відсортувати студентів за ім'ям");
                    SortStudentsByName(items);
                    break;
                default:
                    break;
            }
            start(items);
        }

        private static Student[] ReadStudentsArray() {
            int count = inputInt("скільки студентів ви хочете створити", 1, int.MaxValue);
            Student[] items = new Student[count];
            for (int i = 0; i < count; i++) {
                printMenuTitle($"студент No{i + 1}");
                items[i] = ReadStudent();
            }

            return items;
        }

        private static Student ReadStudent() {
            Student it = new Student(
                inpuString("Ім'я студента"),
                inpuString("Прізвище студента"),
                inpuString("Шифр групи"),
                inputInt("Номер курсу", 1, 5)
            );
            if (selectMenuItem(new string[] { "завершити", "додати оцінку" }) == 1) ReadStudentResults(ref it);
            return it;
        }

        private static void ReadStudentResults(ref Student item) {
            item.addResult(new Result(
                inpuString("назва предмета"),
                inpuString("П.І.Б. викладача"),
                inputInt("оцінку за 100-бальною шкалою", 0, 100)
            ));

            if (selectMenuItem(new string[] { "завершити", "додати оцінку" }) == 1) ReadStudentResults(ref item);
        }

        private static void PrintStudents(Student[] items) {
            if (items == null || items.Length < 1) {
                writeLine("Список студентів порожній!!!", hintColor);
                return;
            }

            for (int i = 0; i < items.Length; i++) PrintStudent(items, i + 1);
        }

        private static void PrintStudent(Student[] items, int number) {
            if (items == null || items.Length < 1 || number < 1 || number > items.Length) return;
            
            Student it = items[number - 1];
            printTitle($"студент No{number}", 40, hintColor);
            PrintStudent(it);
            printTitle("", 40, hintColor);
        }

        private static void PrintStudent(Student item) {
            writeLine(item.ToString());
        }

        private static void GetStudentsInfo(Student[] items, out double minPoint, out double maxPoint) {
            double min = 100;
            double max = 0;

            foreach (Student it in items) {
                double averagePoints = it.GetAveragePoints();
                if (double.IsNaN(averagePoints)) continue;
                if (averagePoints > max) max = averagePoints;
                if (averagePoints < min) min = averagePoints;
            }

            minPoint = min;
            maxPoint = max;
        }

        private static void SortStudentsByPoints(Student[] items) {
            Array.Sort(items, SortInfoByAveragePoints);
            PrintStudents(items);
        }

        private static int SortInfoByAveragePoints(Student first, Student second) {
            double a = first.GetAveragePoints();
            double b = second.GetAveragePoints();
            return a > b ? 1 : a < b ? -1 : 0;
        }

        private static void SortStudentsByName(Student[] items) {
            Array.Sort(items, SortInfoByName);
            PrintStudents(items);
        }

        private static int SortInfoByName(Student first, Student second) {
            int value = string.Compare(first.surename, second.surename, StringComparison.CurrentCultureIgnoreCase);
            return value == 0 ? string.Compare(first.name, second.name, StringComparison.CurrentCultureIgnoreCase) : value;
        }

        private static int inputInt(string name, int min, int max) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                writeLine($"\n{name} не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
                return inputInt(name, min, max);
            }

            try {
                int n = int.Parse(value);

                if (n < min) writeLine($"\nЗначення не може бути меншим за {min}. Будь ласка, повторіть спробу!!!", errorColor);
                else if (n > max) writeLine($"\nЗначення не може бути білшим за {max}. Будь ласка, повторіть спробу!!!", errorColor);
                else return n;
                
                return inputInt(name, min, max);
            } catch (System.Exception e) {
                writeLine();
                writeLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, повторіть спробу!!!",
                    errorColor
                );
                return inputInt(name, min, max);
            }

        }

        private static string inpuString(string name) {
            writeLine($"Вкажіть {name} та натисніть Enter");
            string value = Console.ReadLine();
            if (!string.IsNullOrEmpty(value)) return value;

            writeLine("Значення не може бути порожнім. Будь ласка, повторіть спробу!!!", errorColor);
            return inpuString(name);
        }

        private static int selectMenuItem(string[] items, int def = -1) {
            if (items == null || items.Length == 0) return def;

            writeLine("\nВкажіть номер пункту та натисніть Enter");

            for (int i = 1; i < items.Length; i++) printMenuItem(i, items[i]);
            printMenuItem(0, items[0]);
            try {
                int it = int.Parse(Console.ReadLine());
                if (it < items.Length) return it;
                throw new System.FormatException();
            } catch (Exception) {
                writeLine("\nЗробіть свій вибір!!!", errorColor);
            }

            return selectMenuItem(items, def);
        }

        private static void printMenuItem(int number, string name) {
            write($"\t{number}", hintColor);
            writeLine($" -> {name}.");
        }

        private static void printMenuTitle(string name) {
            printTitle(name, 120, titleColor);
        }

        private static void printTitle(string name, int spaseLenght, ConsoleColor color = defTextColor) {
            string it = $"----------{name}";
            for (int i = it.Length; i < spaseLenght; i++) it += "-";
            writeLine();
            writeLine(it, color);
            writeLine();
        }

        private static void writeLine(string value = "", ConsoleColor color = defTextColor) {
            write($"{value}\n", color);
        }

        private static void write(string value = "", ConsoleColor color = defTextColor) {
            Console.ForegroundColor = color;
            Console.Write(value);
            Console.ForegroundColor = defTextColor;
        }


        struct Result {

            public string subject;
            public string teacher;
            public int points;

            public Result(string subject, string teacher, int points) {
                this.subject = subject;
                this.teacher = teacher;
                this.points = points > 100 ? 100 : points < 0 ? 0 : points;
            }

            public Result(string subject, string teacher) {
                this.subject = subject;
                this.teacher = teacher;
                points = 0;
            }

            public override string ToString() {
                return $"{subject}, оцінка {points} балів, викладач {teacher}";
            }
        }

        struct Student {

            public string name;
            public string surename;
            public string group;
            public int year;
            public Result[] results;

            public Student(string name, string surename, string group, int year, Result[] results) {
                this.name = name;
                this.surename = surename;
                this.group = group;
                this.year = year;
                this.results = results;
            }

            public Student(string name, string surename, string group, int year) {
                this.name = name;
                this.surename = surename;
                this.group = group;
                this.year = year;
                this.results = new Result[0];
            }

            public void addResult(Result result) {
                Result[] items = new Result[results == null || results.Length == 0 ? 1 : results.Length + 1];
                if (results != null && results.Length > 0) results.CopyTo(items, 0);
                items[items.Length - 1] = result;
                results = items;

                Console.WriteLine(results.Length);
            }

            public double GetAveragePoints() {
                if (results == null || results.Length == 0) return double.NaN;
                int sum = 0;
                foreach (Result it in results) sum += it.points;
                return Math.Round((sum / results.Length) * 1.0, 2);
            }

            public string GetBestSubject() {
                if (results == null || results.Length == 0) return "У студента немає оцінок!!!";

                int maxPoints = 0;
                string subject = "У студента по всім предметам 0 балів";

                foreach (Result it in results) {
                    if (it.points <= maxPoints) continue;

                    maxPoints = it.points;
                    subject = it.subject;
                }

                return subject;
            }

            public string GetWorstSubject() {
                if (results == null || results.Length == 0) return "У студента немає оцінок!!!";

                int minPoints = 100;
                string subject = "У студента по всім предметам 100 балів";

                foreach (Result it in results) {
                    if (it.points >= minPoints) continue;

                    minPoints = it.points;
                    subject = it.subject;
                }

                return subject;
            }

            public override string ToString() {
                string value = "Студент:\n";
                value += $"\tІм'я : {name}\n";
                value += $"\tПрізвище : {surename}\n";
                value += $"\tШифр групи : {group}\n";
                value += $"\tНомер курсу : {year}\n";

                value += "\tРезультати сесії :" + (results.Length > 0 ? "" : " відсутні") + "\n";
                foreach (Result it in results) value += $"\t\t{it.ToString()}\n";

                double averagePoints = GetAveragePoints();
                value += $"\tСередній бал : {(double.IsNaN(averagePoints) ? "У студента немає оцінок!!!" : averagePoints.ToString())}\n";
                value += $"\tПредмет з найвищим балом : {GetBestSubject()}\n";
                value += $"\tПредмет з найгіршим балом : {GetWorstSubject()}\n";

                return value;
            }

        }
    }
}
